# shiny-sxyqtl

Shiny server for sxyqtl

sxyqtl.shiny.embl.de

## Building image
docker build -t git.embl.de:4567/brooks/shiny-sxyqtl:v1 .

## Push it
docker push  git.embl.de:4567/brooks/shiny-sxyqtl:v1
