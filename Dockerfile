FROM rocker/shiny:3.5.1

RUN apt-get install libssl-dev libxml2-dev -y \
     && rm -rf /var/lib/apt/lists/*

#ADD app /srv/shiny-server/
ADD etc /etc/shiny-server/
ADD packages.R /tmp/packages.R
RUN Rscript /tmp/packages.R
